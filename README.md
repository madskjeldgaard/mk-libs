# mk-libs

This is a meta-package for SuperCollider that installs all the necessary sclang dependencies for a full Mads Kjeldgaard system :)

## Packages contained in this meta package
- [https://codeberg.org/madskjeldgaard/mk-synthlib](https://codeberg.org/madskjeldgaard/mk-synthlib)
- [https://codeberg.org/madskjeldgaard/mk-fxlib](https://codeberg.org/madskjeldgaard/mk-fxlib)
- [https://codeberg.org/madskjeldgaard/mk-misclib](https://codeberg.org/madskjeldgaard/mk-misclib)
- [https://github.com/madskjeldgaard/fxpatterns](https://github.com/madskjeldgaard/fxpatterns)
- [https://github.com/madskjeldgaard/PolyBuf](https://github.com/madskjeldgaard/PolyBuf)

## Requirements

[Faust needs to be installed](https://github.com/grame-cncm/faust) and at least version 2.40. Make sure that `faust2sc.py` is executable and in your path.

## Installation

```supercollider
Quarks.install("https://codeberg.org/madskjeldgaard/mk-libs")
```

On the first class library compilation after installation, the Faust plugins included in the packages will be automatically compiled and installed. You will need to recompile once more once it's done.
